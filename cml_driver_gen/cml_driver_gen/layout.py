from abs_templates_ec.analog_core import AnalogBase
from bag.layout.routing import TrackManager, TrackID
from .params import Cml_driverParams

from sal.transistor import *


class layout(AnalogBase):
    """
    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        AnalogBase.__init__(self, temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None
    
    @classmethod
    def get_params_info(cls):
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='Cml_driverParams parameter object',
        )

    @classmethod
    def get_default_param_values(cls):
        """
        Returns a dictionary containing default parameter values.

        Override this method to define default parameter values.  As good practice,
        you should avoid defining default values for technology-dependent parameters
        (such as channel length, transistor width, etc.), but only define default
        values for technology-independent parameters (such as number of tracks).

        Returns
        -------
        default_params : dict[str, any]
           dictionary of default parameter values.
        """
        return dict(
        )

    @property
    def sch_params(self):
        # type: () -> Dict[str, Any]
        return self._sch_params

    def draw_layout(self):
        params: Cml_driverParams = self.params['params']

        # horiz_conn_layer = self.mos_conn_layer + 1
        vert_conn_layer = self.mos_conn_layer + 2

        tr_manager = TrackManager(self.grid, params.tr_widths, params.tr_spaces)

        # Set up the row information
        row_Dummy_NB = Row(name='Dummy_NB',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.N,
                           width=params.w_dict['Dummy_NB'],
                           threshold=params.th_dict['Dummy_NB'],
                           wire_names_g=['sig1', 'DMY'],
                           wire_names_ds=['DMY']
                           )

        row_nmos_input = Row(name='nmos_input',
                             orientation=RowOrientation.R0,
                             channel_type=ChannelType.N,
                             width=params.w_dict['n'],
                             threshold=params.th_dict['n'],
                             wire_names_g=['IN0', 'IN1'],
                             wire_names_ds=['VSS', 'INT']
                             )

        row_nmos_cas0 = Row(name='nmos_cas0',
                            orientation=RowOrientation.R0,
                            channel_type=ChannelType.N,
                            width=params.w_dict['n'],
                            threshold=params.th_dict['n'],
                            wire_names_g=['CAS'],
                            wire_names_ds=['INT', 'OUT']
                            )

        row_nmos_cas1 = Row(name='nmos_cas1',
                            orientation=RowOrientation.R0,
                            channel_type=ChannelType.N,
                            width=params.w_dict['n'],
                            threshold=params.th_dict['n'],
                            wire_names_g=['CAS'],
                            wire_names_ds=['INT', 'OUT']
                            )

        row_DUMMY_NT = Row(name='Dummy_NT',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.N,
                           width=params.w_dict['Dummy_NT'],
                           threshold=params.th_dict['Dummy_NT'],
                           wire_names_g=['DMY'],
                           wire_names_ds=['DMY0', 'DMY1']
                           )

        row_Dummy_PT = Row(name='Dummy_PT',
                           orientation=RowOrientation.R0,
                           channel_type=ChannelType.P,
                           width=params.w_dict['Dummy_PT'],
                           threshold=params.th_dict['Dummy_PT'],
                           wire_names_g=['ENB'],
                           wire_names_ds=['I', 'O']
                           )


        # Define the order of the rows (bottom to top) for this analogBase cell
        rows = RowList([row_Dummy_NB, row_nmos_input, row_nmos_cas0, row_nmos_cas1, row_DUMMY_NT, row_Dummy_PT])

        # Initialize the transistors in the design
        divide = 1
        fg_n = params.seg_dict['n'] // divide

        # Define the number of bits of CML driver
        bit = 4
        fg_list = [2, 4, 8, 16]
        transistors = []
        for b in range(bit):
            transistors.append(Transistor(name='n', row=row_nmos_input, fg=fg_list[b]))
            transistors.append(Transistor(name='n', row=row_nmos_cas0, fg=fg_list[b]))
            transistors.append(Transistor(name='n', row=row_nmos_cas1, fg=fg_list[b]))

        # Compose a list of all the transistors so it can be iterated over later

        # 3:   Calculate transistor locations
        fg_space = 8
        fg_total = sum(fg_list) + 2 * params.ndum + (bit-1) * fg_space
        fg_dum = params.ndum

        #if (pmos.fg - nmos.fg) % 4 != 0:
        #    raise ValueError('We assume seg_p and seg_n differ by multiples of 4.')

        # Calculate positions of transistors
        offset = [fg_dum, fg_dum+fg_list[0]+fg_space, fg_dum+fg_list[0]+fg_list[1]+2*fg_space, fg_dum+fg_list[0]+fg_list[1]+fg_list[2]+3*fg_space]
        for idx, tx in enumerate(transistors):
            tx.assign_column(offset=offset[idx//3], fg_col=tx.fg, align=TransistorAlignment.CENTER)
            if tx.row.name == 'nmos_input':
                tx.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
            else:
                tx.set_directions(seff=EffectiveSource.D, seff_dir=TransistorDirection.DOWN)

        # 4:  Assign the transistor directions (s/d up vs down)

        n_rows = rows.n_rows
        p_rows = rows.p_rows

        # 5:  Draw the transistor rows, and the transistors
        # Draw the transistor row bases
        self.draw_base(params.lch, fg_total, params.ptap_w, params.ntap_w,
                       n_rows.attribute_values('width'), n_rows.attribute_values('threshold'),
                       p_rows.attribute_values('width'), p_rows.attribute_values('threshold'),
                       tr_manager=tr_manager,
                       wire_names=rows.wire_names_dict(),
                       n_orientations=n_rows.attribute_values('orientation'),
                       p_orientations=p_rows.attribute_values('orientation'),
                       top_layer=params.top_layer,
                       half_blk_x=True, half_blk_y=True,
                       guard_ring_nf=params.guard_ring_nf,
                       )

        # Draw the transistors

        for tx in transistors:
            ports = self.draw_mos_conn(mos_type=tx.row.channel_type.value,
                                       row_idx=rows.index_of_same_channel_type(tx.row),
                                       col_idx=tx.col,
                                       fg=tx.fg,
                                       sdir=tx.s_dir.value,
                                       ddir=tx.d_dir.value,
                                       s_net=tx.s_net,
                                       d_net=tx.d_net,
                                       gate_ext_mode=1,
                                       #g_via_row=2,
                                       )
            tx.set_ports(g=ports['g'],
                         d=ports[tx.deff.value],
                         s=ports[tx.seff.value])


        # 6:  Define horizontal tracks on which connections will be made
        row_nmos_idx = rows.index_of_same_channel_type(row_nmos_input)
        #row_pmos_idx = rows.index_of_same_channel_type(row_pmos)
        #tid_input = self.get_wire_id('nch', row_nmos_idx, 'g', wire_name='IN0')
        #tid_pmos_G = self.get_wire_id('pch', row_pmos_idx, 'g', wire_name='ENB')
        #tid_nmos_S = self.get_wire_id('nch', row_nmos_idx, 'ds', wire_name='I')
        #tid_pmos_S = self.get_wire_id('pch', row_pmos_idx, 'ds', wire_name='I')
        #tid_pmos_D = self.get_wire_id('pch', row_pmos_idx, 'ds', wire_name='O')

        # 7:  Perform wiring
        # inputs
        row = [row_nmos_input, row_nmos_cas0, row_nmos_cas1]
        # for idx, tx in enumerate(transistors):
        #     if tx.row.name == 'nmos_input':
        #         tr_gate_idx = rows.index_of_same_channel_type(row[idx % 3])
        #         tid_input = self.get_wire_id('nch', tr_gate_idx, 'g')
        #         route = self.connect_to_tracks([tx.g], tid_input)

        # draw dummies
        ptap_wire_arrs, ntap_wire_arrs = self.fill_dummy()
        # export supplies
        self.add_pin('VSS', ptap_wire_arrs)
        self.add_pin('VDD', ntap_wire_arrs)

        for b in range(bit):

            # inputs
            tr_idx = rows.index_of_same_channel_type(row_nmos_input)
            tid = self.get_wire_id('nch', tr_idx, 'g')
            route = self.connect_to_tracks([transistors[3*b].g], tid)
            self.add_pin('IN<' + str(b) + '>', route, show=params.show_pins)

            # tr_idx = rows.index_of_same_channel_type(row_nmos_input)
            # tid = self.get_wire_id('nch', tr_idx, 'ds')
            # route = self.connect_to_tracks([transistors[3 * b].s, ptap_wire_arrs[0]], tid)

            self.connect_to_substrate('ptap', transistors[3 * b].s)

            # input to cas
            tr_idx = rows.index_of_same_channel_type(row_nmos_input)
            tid = self.get_wire_id('nch', tr_idx, 'ds', wire_name='INT')
            self.connect_to_tracks([transistors[3*b].d, transistors[3*b+1].s], tid)
            self.connect_to_tracks([transistors[3*b].d, transistors[3*b+2].s], tid)

            # cas gate
            # should be M5
            tr_idx = rows.index_of_same_channel_type(row_nmos_cas0)
            tid = self.get_wire_id('nch', tr_idx, 'g')
            cas0 = self.connect_to_tracks([transistors[3*b+1].g], tid)

            tr_idx = rows.index_of_same_channel_type(row_nmos_cas1)
            tid = self.get_wire_id('nch', tr_idx, 'g')
            cas1 = self.connect_to_tracks([transistors[3*b+2].g], tid)
            tr_idx_ver = TrackID(layer_id=vert_conn_layer,
                             track_idx=self.grid.coord_to_nearest_track(
                                 layer_id=vert_conn_layer,
                                 coord=(cas0.lower_unit+cas0.upper_unit)/2,
                                 #coord=cas0.lower_unit,
                                 half_track=False,
                                 mode=1,
                                 unit_mode=True)-(b//2+1),
                             width=tr_manager.get_width(vert_conn_layer, 'sig1'),
                             num=(b + 1),
                             pitch=2
                             )

            route = self.connect_to_tracks([cas0, cas1], tr_idx_ver)
            self.add_pin('CAS<' + str(b) + '>', route, show=params.show_pins)


            # OUTPUT
            tr_idx = rows.index_of_same_channel_type(row_nmos_cas0)
            tid = self.get_wire_id('nch', tr_idx, 'ds', wire_name='OUT')
            out0 = self.connect_to_tracks([transistors[3*b+1].d], tid)

            tr_idx = rows.index_of_same_channel_type(row_nmos_cas1)
            tid = self.get_wire_id('nch', tr_idx, 'ds', wire_name='OUT')
            out1 = self.connect_to_tracks([transistors[3*b+2].d], tid)
            tr_idx_ver2 = TrackID(layer_id=vert_conn_layer,
                             track_idx=self.grid.coord_to_nearest_track(
                                       layer_id=vert_conn_layer,
                                       coord=(out0.lower_unit+out0.upper_unit)/2,
                                       half_track=False,
                                       mode=1,
                                       unit_mode=True)-(b//2+2),
                             width=tr_manager.get_width(vert_conn_layer, 'sig1'),
                             num=(b+2),
                             pitch=2
                             )

            route = self.connect_to_tracks([out0, out1], tr_idx_ver2)
            self.add_pin('OUT:', route, show=params.show_pins)

            # Add Pin
            # inside the for loop



        ptap_wire_arrs, ntap_wire_arrs = self.fill_dummy()
        # Define transistor properties for schematic
        tx_info = {}
        for tx in transistors:
            tx_info[tx.name] = {
                'w': tx.row.width,
                'th': tx.row.threshold,
                'fg': tx.fg
            }

        self._sch_params = dict(
            lch=params.lch,
            dum_info=self.get_sch_dummy_info(),
            tx_info=tx_info,
        )


class cml_driver(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        AnalogBase.__init__(self, temp_db, lib_name, params, used_names, **kwargs)
