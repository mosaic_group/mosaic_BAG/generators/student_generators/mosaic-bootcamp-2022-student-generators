"""
cml_driver
========

"""

from sal.design_base import DesignBase
from .params import Cml_driverParams
from .layout import layout as cml_driver_layout


class design(DesignBase):
    def __init__(self):
        DesignBase.__init__(self)
        self.params = self.parameter_class().defaults(min_lch=self.min_lch)

    @property
    def package(self):
        return "cml_driver_gen"

    @classmethod
    def layout_generator_class(cls):
        """Return the layout generator class"""
        return cml_driver_layout

    @classmethod
    def parameter_class(cls):
        """Return the parameter class"""
        return Cml_driverParams

    # Define template draw and schematic parameters below using property decorators:
    @property
    def params(self) -> Cml_driverParams:
        return self._params

    @params.setter
    def params(self, val: Cml_driverParams):
        self._params = val
