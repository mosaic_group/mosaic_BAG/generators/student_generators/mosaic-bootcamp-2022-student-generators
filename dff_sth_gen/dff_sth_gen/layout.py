import copy
from typing import *

#from abs_templates_ec.analog_core import AnalogBase
from bag.layout.routing import TrackID
from bag.layout.template import TemplateBase

from inv_sth_gen.layout import inv_sth
from nand_sth_gen.layout import nand_sth
from .params import DFF_sthParams

#from sal.transistor import *


class layout(TemplateBase):
    """
    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        TemplateBase.__init__(self, temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None
    
    @classmethod
    def get_params_info(cls):
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='DFF_sthParams parameter object',
        )

    @classmethod
    def get_default_param_values(cls):
        """
        Returns a dictionary containing default parameter values.

        Override this method to define default parameter values.  As good practice,
        you should avoid defining default values for technology-dependent parameters
        (such as channel length, transistor width, etc.), but only define default
        values for technology-independent parameters (such as number of tracks).

        Returns
        -------
        default_params : dict[str, any]
           dictionary of default parameter values.
        """
        return dict(
        )

    @property
    def sch_params(self):
        # type: () -> Dict[str, Any]
        return self._sch_params

    def draw_layout(self):
        # Make copies of given dictionaries to avoid modifying external data.
        params: DFF_sthParams = copy.deepcopy(self.params['params'])
        inv_params = params.inv_params
        nand_params = params.nand_params

        # Disable pins in subcells
        inv_params.show_pins = False
        nand_params.show_pins = False
        # nand_params.tr_widths['sig1'][5] = 2

        # Create layout masters for subcells
        inv_master = self.new_template(temp_cls=inv_sth, params=dict(params=inv_params))
        nand_master = self.new_template(temp_cls=nand_sth, params=dict(params=nand_params))

        # Add subcell instances
        InvWidth = inv_master.bound_box.width_unit
        InvHeight = inv_master.bound_box.height_unit
        NandWidth = nand_master.bound_box.width_unit
        NandHeight = nand_master.bound_box.height_unit

        inv = self.add_instance(inv_master, 'inv0', loc=(0, 0), unit_mode=True)
        nand0 = self.add_instance(nand_master, 'nand0', loc=(InvWidth+0*NandWidth, 0), orient='R0', unit_mode=True)
        nand1 = self.add_instance(nand_master, 'nand1', loc=(InvWidth+1*NandWidth, 0), orient='R0', unit_mode=True)
        nand2 = self.add_instance(nand_master, 'nand2', loc=(InvWidth+0*NandWidth, 2*NandHeight), orient='MX', unit_mode=True)
        nand3 = self.add_instance(nand_master, 'nand3', loc=(InvWidth+1*NandWidth, 2*NandHeight), orient='MX', unit_mode=True)

        # Grab the pin infos of subcells
        nand = [nand0, nand1, nand2, nand3]
        InvInput = inv.get_all_port_pins('I')[0]
        InvOutput = inv.get_all_port_pins('O')[0]
        NandInputA = []
        NandInputB = []
        NandOutput = []
        NandVDD = []
        NandVSS = []
        for nd in nand:
            NandInputA.append(nd.get_all_port_pins('A')[0])
            NandInputB.append(nd.get_all_port_pins('B')[0])
            NandOutput.append(nd.get_all_port_pins('OUT')[0])
            NandVDD.append(nd.get_all_port_pins('VDD')[0])
            NandVSS.append(nd.get_all_port_pins('VSS')[0])

        InvVDD = inv.get_all_port_pins('VDD')[0]
        InvVSS = inv.get_all_port_pins('VSS')[0]

        # get layer IDs
        HorLayer = [4, 6]
        VerLayer = 5
        TopLayer = 7

        # get vertical TrackIDs
        TopLayerWidth = self.grid.get_track_width(TopLayer, 1, unit_mode=True)
        VerLayerWidth = self.grid.get_min_track_width(VerLayer, top_w=TopLayerWidth, unit_mode=True)
        HorLayerWidth = [0, 0]
        HorLayerWidth[0] = self.grid.get_min_track_width(HorLayer[0], top_w=TopLayerWidth, unit_mode=True)
        HorLayerWidth[1] = self.grid.get_min_track_width(HorLayer[1], top_w=TopLayerWidth, unit_mode=True)

        # Perform routings
        track = TrackID(HorLayer[1], self.grid.coord_to_nearest_track(HorLayer[1], (NandInputA[0].middle + NandInputB[2].middle) / 2), width=HorLayerWidth[1])
        RouteCLK = self.connect_to_tracks([NandInputA[0], NandInputB[2]], track)
        self.add_pin('CLK', RouteCLK, show=params.show_pins)

        track = TrackID(HorLayer[1], self.grid.coord_to_nearest_track(HorLayer[1], InvOutput.middle), width=HorLayerWidth[1])
        self.connect_to_tracks([InvOutput, NandInputB[0]], track)

        track = TrackID(HorLayer[1], self.grid.coord_to_nearest_track(HorLayer[1], (NandInputA[2].middle+InvInput.middle)/2+2), width=HorLayerWidth[1])
        RouteCLK = self.connect_to_tracks([NandInputA[2], InvInput], track)
        self.add_pin('D', RouteCLK, show=params.show_pins)

        track = TrackID(HorLayer[1], self.grid.coord_to_nearest_track(HorLayer[1], NandOutput[0].lower), width=HorLayerWidth[1])
        self.connect_to_tracks([NandOutput[0], NandInputB[1]], track)

        track = TrackID(HorLayer[1], self.grid.coord_to_nearest_track(HorLayer[1], NandOutput[2].lower), width=HorLayerWidth[1])
        self.connect_to_tracks([NandOutput[2], NandInputA[3]], track)

        track = TrackID(HorLayer[1], self.grid.coord_to_nearest_track(HorLayer[1], (NandOutput[3].middle+NandInputA[1].middle)/2), width=HorLayerWidth[1])
        RouteQ = self.connect_to_tracks([NandOutput[3], NandInputA[1]], track)
        self.add_pin('Q:', RouteQ, show=params.show_pins)

        track = TrackID(HorLayer[1], self.grid.coord_to_nearest_track(HorLayer[1], NandOutput[1].upper), width=HorLayerWidth[1])
        RouteQB = self.connect_to_tracks([NandOutput[1], NandInputB[3]], track)
        self.add_pin('QB:', RouteQB, show=params.show_pins)

        # connect VDD/VSS wires with M4 and generate pins
        RouteVDD0 = self.connect_wires([InvVDD, NandVDD[1]], lower=InvVDD.lower, upper=NandVDD[1].upper)
        RouteVSS0 = self.connect_wires([InvVSS, NandVSS[1]], lower=InvVSS.lower, upper=NandVSS[1].upper)
        RouteVDD1 = self.connect_wires([NandVDD[2], NandVDD[3]], lower=NandVDD[2].lower, upper=NandVDD[3].upper)
        RouteVSS1 = self.connect_wires([NandVSS[2], NandVSS[3]], lower=NandVSS[2].lower, upper=NandVSS[3].upper)

        self.add_pin('VDD:', [RouteVDD0[0], RouteVDD1[0]], show=params.show_pins)
        self.add_pin('VSS:', [RouteVSS0[0], RouteVSS1[0]], show=params.show_pins)

        # Generate Q/QB pins on the original points of each nand gete
        self.reexport(nand1.get_port('OUT'), net_name='QB:', show=params.show_pins)
        self.reexport(nand3.get_port('OUT'), net_name='Q:', show=params.show_pins)

        # Save the schematic parameters
        self._sch_params = dict(
            inv_params = inv_master.sch_params,
            nand_params = nand_master.sch_params
        )


class dff_sth(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        AnalogBase.__init__(self, temp_db, lib_name, params, used_names, **kwargs)
