#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import ParamsBase

from inv_sth_gen.params import Inv_sthParams
from nand_sth_gen.params import Nand_sthParams

@dataclass
class DFF_sthParams(ParamsBase):
    """
    Parameter class for dff_sth_gen

    Args:
    ----
    ntap_w : Union[float, int]
        Width of the N substrate contact

    ptap_w : Union[float, int]
        Width of P substrate contact

    lch : float
        Channel length of the transistors

    th_dict : Dict[str, str]
        NMOS/PMOS threshold flavor dictionary

    ndum : int
        Number of fingers in NMOS transistor

    tr_spaces : Dict[str, int]
        Track spacing dictionary

    tr_widths : Dict[str, Dict[int, int]]
        Track width dictionary

    top_layer: int
        Top metal Layer used in Layout

    ndum_mid_stages : int
        number of dummy fingers/transistors between stages/columns of transistors

    w_dict : Dict[str, Union[float, int]]
        NMOS/PMOS width dictionary

    seg_dict : Dict[str, int]
        NMOS/PMOS number of segments dictionary

    guard_ring_nf : int
        Width of guard ring

    show_pins : bool
        True to create pin labels

    out_conn : str
        Enable Alternative structure for flipped well processes

    finfet : bool
        Enable finfet process
    """

    inv_params: Inv_sthParams
    nand_params: Nand_sthParams
    show_pins: bool

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> DFF_sthParams:
        return DFF_sthParams(
            inv_params=Inv_sthParams.finfet_defaults(min_lch),
            nand_params=Nand_sthParams.finfet_defaults(min_lch),
            show_pins=True
                           )

    @classmethod
    def planar_defaults(cls, min_lch: float) -> DFF_sthParams:
        return DFF_sthParams(
            inv_params=Inv_sthParams.planar_defaults(min_lch),
            nand_params=Nand_sthParams.planar_defaults(min_lch),
            show_pins=True
        )
