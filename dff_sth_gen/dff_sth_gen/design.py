"""
dff_sth
========

"""

from sal.design_base import DesignBase
from .params import DFF_sthParams
from .layout import layout as dff_sth_layout


class design(DesignBase):
    def __init__(self):
        DesignBase.__init__(self)
        self.params = self.parameter_class().defaults(min_lch=self.min_lch)

    @property
    def package(self):
        return "dff_sth_gen"

    @classmethod
    def layout_generator_class(cls):
        """Return the layout generator class"""
        return dff_sth_layout

    @classmethod
    def parameter_class(cls):
        """Return the parameter class"""
        return DFF_sthParams

    # Define template draw and schematic parameters below using property decorators:
    @property
    def params(self) -> DFF_sthParams:
        return self._params

    @params.setter
    def params(self, val: DFF_sthParams):
        self._params = val
