import os
from typing import *
from bag.design import Module

yaml_file = os.path.join(f'{os.environ["BAG_GENERATOR_ROOT"]}/BagModules/dff_sth_templates', 'netlist_info', 'dff_sth.yaml')


# noinspection PyPep8Naming
class schematic(Module):
    """Module for library dff_sth_templates cell dff_sth.

    Fill in high level description here.
    """

    def __init__(self, bag_config, parent=None, prj=None, **kwargs):
        Module.__init__(self, bag_config, yaml_file, parent=parent, prj=prj, **kwargs)
       
    @classmethod
    def get_params_info(cls):
        # type: () -> Dict[str, str]
        """Returns a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : Optional[Dict[str, str]]
            dictionary from parameter names to descriptions.
        """
        return dict(
            inv_params='inverter/switch channel length, in meters.',
            nand_params='List of dictionaries of transistor information',
            # dum_info='Dummy information data structure.',
            )

    def design(self, **kwargs):
        """To be overridden by subclasses to design this module.

        This method should fill in values for all parameters in
        self.parameters.  To design instances of this module, you can
        call their design() method or any other ways you coded.

        To modify schematic structure, call:

        rename_pin()
        delete_instance()
        replace_instance_master()
        reconnect_instance_terminal()
        restore_instance()
        array_instance()
        """

        inv_params = kwargs.get('inv_params')
        nand_params = kwargs.get('nand_params')

        self.instances['inv0'].design(**inv_params)
        self.instances['nand0'].design(**nand_params)
        self.instances['nand1'].design(**nand_params)
        self.instances['nand2'].design(**nand_params)
        self.instances['nand3'].design(**nand_params)

        # design dummies
        # self.design_dummy_transistors(dum_info, 'XDUM', 'VDD', 'VSS')
