from abs_templates_ec.analog_core import AnalogBase
from bag.layout.routing import TrackManager, TrackID
from .params import NANDParams

from sal.transistor import *


class layout(AnalogBase):
    """
    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        AnalogBase.__init__(self, temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None
    
    @classmethod
    def get_params_info(cls):
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='NANDParams parameter object',
        )

    @classmethod
    def get_default_param_values(cls):
        """
        Returns a dictionary containing default parameter values.

        Override this method to define default parameter values.  As good practice,
        you should avoid defining default values for technology-dependent parameters
        (such as channel length, transistor width, etc.), but only define default
        values for technology-independent parameters (such as number of tracks).

        Returns
        -------
        default_params : dict[str, any]
           dictionary of default parameter values.
        """
        return dict(
        )

    @property
    def sch_params(self):
        # type: () -> Dict[str, Any]
        return self._sch_params

    def draw_layout(self):
        params: NANDParams = self.params['params']



        tr_manager = TrackManager(self.grid, params.tr_widths, params.tr_spaces)

        row_nmos = Row(name='n',
                       orientation=RowOrientation.MX,
                       channel_type=ChannelType.N,
                       width=params.w_dict['n'],
                       threshold=params.th_dict['n'],
                       wire_names_g=['A', 'B'],
                       wire_names_ds=['O', 'NCDS', 'VSS']
                       )

        row_pmos = Row(name='p',
                       orientation=RowOrientation.R0,
                       channel_type=ChannelType.P,
                       width=params.w_dict['p'],
                       threshold=params.th_dict['p'],
                       wire_names_g=['A', 'B'],
                       wire_names_ds=['O', 'VDD']
                       )

        # Define the order of the rows (bottom to top) for this analogBase cell
        rows = RowList([row_nmos, row_pmos])

        # Initialize the transistors in the design
        divide = 1
        fg_n = params.seg_dict['n'] // divide
        fg_p = params.seg_dict['p'] // divide

        nmos0 = Transistor(name='n0', row=row_nmos, fg=fg_n, seff_net='VSS',  deff_net='NCDS')
        nmos1 = Transistor(name='n1', row=row_nmos, fg=fg_n, seff_net='NCDS', deff_net='O')
        pmos0 = Transistor(name='p0', row=row_pmos, fg=fg_p, seff_net='VDD',  deff_net='O')
        pmos1 = Transistor(name='p1', row=row_pmos, fg=fg_p, seff_net='VDD',  deff_net='O')



        # 3:   Calculate transistor locations
        fg_in_spacing = 4
        fg_single = max(nmos0.fg, pmos0.fg)
        fg_total = 2*fg_single + 2 * params.ndum + fg_in_spacing
        fg_dum = params.ndum

        # Calculate positions of transistors
        nmos0.assign_column(offset=fg_dum, fg_col=fg_single,                   align=TransistorAlignment.CENTER)
        nmos1.assign_column(offset=fg_dum, fg_col=(3*fg_single+fg_in_spacing), align=TransistorAlignment.CENTER)
        pmos0.assign_column(offset=fg_dum, fg_col=fg_single,                   align=TransistorAlignment.CENTER)
        pmos1.assign_column(offset=fg_dum, fg_col=(3*fg_single+fg_in_spacing), align=TransistorAlignment.CENTER)

        # 4:  Assign the transistor directions (s/d up vs down)
        nmos0.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.UP)
        nmos1.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.UP)
        pmos0.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.UP)
        pmos1.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.UP)

        # Compose a list of all the transistors so it can be iterated over later
        transistors = [nmos0, nmos1, pmos0, pmos1]

        n_rows = rows.n_rows
        p_rows = rows.p_rows

        # 5:  Draw the transistor rows, and the transistors
        # Draw the transistor row bases
        self.draw_base(params.lch, fg_total, params.ptap_w, params.ntap_w,
                       n_rows.attribute_values('width'), n_rows.attribute_values('threshold'),
                       p_rows.attribute_values('width'), p_rows.attribute_values('threshold'),
                       tr_manager=tr_manager, wire_names=rows.wire_names_dict(),
                       n_orientations=n_rows.attribute_values('orientation'),
                       p_orientations=p_rows.attribute_values('orientation'),
                       top_layer=params.top_layer,
                       half_blk_x=True, half_blk_y=True,
                       guard_ring_nf=params.guard_ring_nf,
                       )

        # Draw the transistors
        for tx in transistors:
            ports = self.draw_mos_conn(mos_type=tx.row.channel_type.value,
                                       row_idx=rows.index_of_same_channel_type(tx.row),
                                       col_idx=tx.col,
                                       fg=tx.fg,
                                       sdir=tx.s_dir.value,
                                       ddir=tx.d_dir.value,
                                       s_net=tx.s_net,
                                       d_net=tx.d_net,
                                       gate_ext_mode=1,
                                       g_via_row=2,
                                       )
            tx.set_ports(g=ports['g'],
                         d=ports[tx.deff.value],
                         s=ports[tx.seff.value])

        # 6:  Define horizontal tracks on which connections will be made
        row_nmos_idx = rows.index_of_same_channel_type(row_nmos)
        row_pmos_idx = rows.index_of_same_channel_type(row_pmos)

        # Connect all gates first. That means connect all N0 gates first.
        # Then N1, then P0, and finally P1.
        tid_nmos0_G = self.get_wire_id('nch', row_nmos_idx, 'g', wire_name='A')
        tid_nmos1_G = self.get_wire_id('nch', row_nmos_idx, 'g', wire_name='B')
        tid_pmos0_G = self.get_wire_id('pch', row_pmos_idx, 'g', wire_name='A')
        tid_pmos1_G = self.get_wire_id('pch', row_pmos_idx, 'g', wire_name='B')

        warr_n0_G = self.connect_to_tracks([nmos0.g], tid_nmos0_G)
        warr_n1_G = self.connect_to_tracks([nmos1.g], tid_nmos1_G)
        warr_p0_G = self.connect_to_tracks([pmos0.g], tid_pmos0_G)
        warr_p1_G = self.connect_to_tracks([pmos1.g], tid_pmos1_G)
        warr_A = self.connect_to_tracks([pmos0.g, nmos0.g], tid_nmos0_G)
        warr_B = self.connect_to_tracks([pmos1.g, nmos1.g], tid_nmos1_G)

        # Connect the drains of the PMOSs'
        tid_pmos0_D = self.get_wire_id('pch', row_pmos_idx, 'ds', wire_name='O')
        tid_pmos1_D = self.get_wire_id('pch', row_pmos_idx, 'ds', wire_name='O')
        warr_p0_D = self.connect_to_tracks([pmos0.d], tid_pmos0_D)
        warr_p1_D = self.connect_to_tracks([pmos1.d], tid_pmos1_D)
        warr_px_D = self.connect_to_tracks([pmos0.d, pmos1.d], tid_pmos0_D)

        # Connect the sources of the NMOS'
        tid_nmos0_S = self.get_wire_id('nch', row_nmos_idx, 'ds', wire_name='VSS')
        tid_nmos1_S = self.get_wire_id('nch', row_nmos_idx, 'ds', wire_name='NCDS')
        warr_n0_S = self.connect_to_tracks([nmos0.s], tid_nmos0_S)
        warr_n1_S = self.connect_to_tracks([nmos1.s], tid_nmos1_S)

        # Connect the drains of the NMOS'
        tid_nmos0_D = self.get_wire_id('nch', row_nmos_idx, 'ds', wire_name='NCDS')
        tid_nmos1_D = self.get_wire_id('nch', row_nmos_idx, 'ds', wire_name='O')
        warr_n0_D = self.connect_to_tracks([nmos0.d], tid_nmos0_D)
        warr_n1_D = self.connect_to_tracks([nmos1.d], tid_nmos1_D)

        # Connect the drain of NMOS0 to source of NMOS1
        warr_NCDS = self.connect_to_tracks([nmos0.d, nmos1.s], tid_nmos0_D)

        # Connect the drain of NMOS1 to the drain of PMOS1
        # Location for vertical track
        loc = warr_px_D.get_bbox_array(self.grid).right
        idx = self.grid.coord_to_nearest_track(layer_id=5, coord=loc) + 1

        # Vertical track to connect drains
        ver_id = TrackID(layer_id=5, track_idx=idx, width=1)
        out_warr = self.connect_to_tracks([warr_n1_D, warr_px_D], ver_id)

        # Connect them substrates
        self.connect_to_substrate('ptap', [nmos0.s])
        self.connect_to_substrate('ntap', [pmos0.s])
        self.connect_to_substrate('ntap', [pmos1.s])

        # Draw dummies
        ptap_wire_arrs, ntap_wire_arrs = self.fill_dummy()

        # Add them pins
        self.add_pin('A', warr_A, show=params.show_pins)
        self.add_pin('B', warr_B, show=params.show_pins)
        self.add_pin('O', out_warr, show=params.show_pins)
        self.add_pin('VSS', ptap_wire_arrs)
        self.add_pin('VDD', ntap_wire_arrs)

        # Define transistor properties for schematic
        tx_info = {}
        for tx in transistors:
            tx_info[tx.name] = {
                'w': tx.row.width,
                'th': tx.row.threshold,
                'fg': tx.fg
            }

        self._sch_params = dict(
            lch=params.lch,
            dum_info=self.get_sch_dummy_info(),
            tx_info=tx_info,
        )


class nand(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        AnalogBase.__init__(self, temp_db, lib_name, params, used_names, **kwargs)
