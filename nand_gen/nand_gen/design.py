"""
nand
========

"""

from sal.design_base import DesignBase
from .params import NANDParams
from .layout import layout as nand_layout


class design(DesignBase):
    def __init__(self):
        DesignBase.__init__(self)
        self.params = self.parameter_class().defaults(min_lch=self.min_lch)

    @property
    def package(self):
        return "nand_gen"

    @classmethod
    def layout_generator_class(cls):
        """Return the layout generator class"""
        return nand_layout

    @classmethod
    def parameter_class(cls):
        """Return the parameter class"""
        return NANDParams

    # Define template draw and schematic parameters below using property decorators:
    @property
    def params(self) -> NANDParams:
        return self._params

    @params.setter
    def params(self, val: NANDParams):
        self._params = val
