"""
bad_diffpair
========

"""

from sal.design_base import DesignBase
from .params import BadDiffPairParams
from .layout import layout as bad_diffpair_layout


class design(DesignBase):
    def __init__(self):
        DesignBase.__init__(self)
        self.params = self.parameter_class().defaults(min_lch=self.min_lch)

    @property
    def package(self):
        return "bad_diffpair_gen"

    @classmethod
    def layout_generator_class(cls):
        """Return the layout generator class"""
        return bad_diffpair_layout

    @classmethod
    def parameter_class(cls):
        """Return the parameter class"""
        return BadDiffPairParams

    # Define template draw and schematic parameters below using property decorators:
    @property
    def params(self) -> BadDiffPairParams:
        return self._params

    @params.setter
    def params(self, val: BadDiffPairParams):
        self._params = val
