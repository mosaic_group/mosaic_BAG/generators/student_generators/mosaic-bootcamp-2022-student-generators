from abs_templates_ec.analog_core import AnalogBase
from bag.layout.routing import TrackManager, TrackID
from .params import BadDiffPairParams

from sal.transistor import *


class layout(AnalogBase):
    """
    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        AnalogBase.__init__(self, temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None
    
    @classmethod
    def get_params_info(cls):
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='BadDiffPairParams parameter object',
        )

    @classmethod
    def get_default_param_values(cls):
        """
        Returns a dictionary containing default parameter values.

        Override this method to define default parameter values.  As good practice,
        you should avoid defining default values for technology-dependent parameters
        (such as channel length, transistor width, etc.), but only define default
        values for technology-independent parameters (such as number of tracks).

        Returns
        -------
        default_params : dict[str, any]
           dictionary of default parameter values.
        """
        return dict(
        )

    @property
    def sch_params(self):
        # type: () -> Dict[str, Any]
        return self._sch_params

    def draw_layout(self):
        params: BadDiffPairParams = self.params['params']

        # horiz_conn_layer = self.mos_conn_layer + 1
        vert_conn_layer = self.mos_conn_layer + 2

        tr_manager = TrackManager(self.grid, params.tr_widths, params.tr_spaces)

        # Set up the row information

        row_nmos1 = Row(name='n1',
                       orientation=RowOrientation.MX,
                       channel_type=ChannelType.N,
                       width=params.w_dict['n'],
                       threshold=params.th_dict['n'],
                       wire_names_g=['VBN1'],
                       wire_names_ds=['N1D', 'VSS']
                       )

        row_nmos2 = Row(name='n2',
                       orientation=RowOrientation.MX,
                       channel_type=ChannelType.N,
                       width=params.w_dict['n'],
                       threshold=params.th_dict['n'],
                       wire_names_g=['VBN2'],
                       wire_names_ds=['VOUT', 'N2S']
                       )

        row_pmos_main = Row(name='pmain',
                       orientation=RowOrientation.R0,
                       channel_type=ChannelType.P,
                       width=params.w_dict['p'],
                       threshold=params.th_dict['p'],
                       wire_names_g=['VIP', 'VIN'],
                       wire_names_ds=['PMAIND', 'PMAINS']
                       )

        row_pmos1 = Row(name='p1',
                       orientation=RowOrientation.R0,
                       channel_type=ChannelType.P,
                       width=params.w_dict['p'],
                       threshold=params.th_dict['p'],
                       wire_names_g=['VBP1'],
                       wire_names_ds=['VOUT', 'P1S', 'PTAIL1D', 'PTAIL1S']
                       )

        row_pmos2 = Row(name='p2',
                       orientation=RowOrientation.R0,
                       channel_type=ChannelType.P,
                       width=params.w_dict['p'],
                       threshold=params.th_dict['p'],
                       wire_names_g=['VBP2'],
                       wire_names_ds=['P2D', 'VDD', 'PTAIL2D', 'PTAIL2S']
                       )


        # Define the order of the rows (bottom to top) for this analogBase cell
        rows = RowList([row_nmos1, row_nmos2, row_pmos_main, row_pmos1, row_pmos2])

        # Initialize the transistors in the design
        # Guide for fingers from params - seg_dict={'n1': 20, 'n2': 10, 'pmain': 20, 'ptail': 40, 'p1': 20, 'p2': 20},
        fg_n1    = params.seg_dict['n1']
        fg_n2    = params.seg_dict['n2']
        fg_pmain = params.seg_dict['pmain']
        fg_ptail = params.seg_dict['ptail']
        fg_p1    = params.seg_dict['p1']
        fg_p2    = params.seg_dict['p2']


        nmos1_1 = Transistor(name='n', row=row_nmos1, fg=fg_n1, seff_net='VSS', deff_net='N1D' )
        nmos1_2 = Transistor(name='n', row=row_nmos1, fg=fg_n1, seff_net='VSS', deff_net='N1D' )

        nmos2_1 = Transistor(name='n', row=row_nmos2, fg=fg_n2, seff_net='N2S', deff_net='VOUT')
        nmos2_2 = Transistor(name='n', row=row_nmos2, fg=fg_n2, seff_net='N2S', deff_net='VOUT')

        pmos_main_1 = Transistor(name='p', row=row_pmos_main, fg=fg_pmain, seff_net='PMAINS', deff_net='PMAIND')
        pmos_main_2 = Transistor(name='p', row=row_pmos_main, fg=fg_pmain, seff_net='PMAINS', deff_net='PMAIND')

        pmos1_1    = Transistor(name='p', row=row_pmos1, fg=fg_p1,    seff_net='P1S', deff_net='VOUT')
        pmos1_tail = Transistor(name='p', row=row_pmos1, fg=fg_ptail, seff_net='PTAIL1S', deff_net='PTAIL1D')
        pmos1_2    = Transistor(name='p', row=row_pmos1, fg=fg_p1,    seff_net='P1S', deff_net='VOUT')

        pmos2_1    = Transistor(name='p', row=row_pmos2, fg=fg_p2,    seff_net='VDD', deff_net='P1S')
        pmos2_tail = Transistor(name='p', row=row_pmos2, fg=fg_ptail, seff_net='PTAIL2S', deff_net='PTAIL2D')
        pmos2_2    = Transistor(name='p', row=row_pmos2, fg=fg_p2,    seff_net='VDD', deff_net='P1S')


        # Compose a list of all the transistors so it can be iterated over later
        transistors = [
            nmos1_1, nmos1_2,
            nmos2_1, nmos2_2,
            pmos_main_1, pmos_main_2,
            pmos1_1, pmos1_tail, pmos1_2,
            pmos2_1, pmos2_tail, pmos2_2
        ]

        # 3:   Calculate transistor locations
        fg_dum = params.ndum
        #fg_single = max(nmos.fg, pmos.fg)
        fg_total = fg_dum + fg_p1 + fg_ptail + fg_p2 + fg_dum

        # Calculate positions of transistors for top PMOS first
        pmos2_1.assign_column(offset=fg_dum, fg_col=fg_p1, align=TransistorAlignment.CENTER)
        pmos2_tail.assign_column(offset=fg_dum, fg_col=3*fg_p1+fg_ptail, align=TransistorAlignment.CENTER)
        pmos2_2.assign_column(offset=fg_dum, fg_col=3*fg_p1+fg_ptail+fg_p1, align=TransistorAlignment.CENTER)

        # 4:  Assign the transistor directions (s/d up vs down)
        # nmos.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        # pmos.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.UP)
        pmos2_1.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.UP)
        pmos2_tail.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.UP)
        pmos2_2.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.UP)

        n_rows = rows.n_rows
        p_rows = rows.p_rows

        # 5:  Draw the transistor rows, and the transistors
        # Draw the transistor row bases
        self.draw_base(params.lch, fg_total, params.ptap_w, params.ntap_w,
                       n_rows.attribute_values('width'), n_rows.attribute_values('threshold'),
                       p_rows.attribute_values('width'), p_rows.attribute_values('threshold'),
                       tr_manager=tr_manager, wire_names=rows.wire_names_dict(),
                       n_orientations=n_rows.attribute_values('orientation'),
                       p_orientations=p_rows.attribute_values('orientation'),
                       top_layer=params.top_layer,
                       half_blk_x=True, half_blk_y=True,
                       guard_ring_nf=params.guard_ring_nf,
                       )

        # Draw the transistors
        for tx in transistors:
            ports = self.draw_mos_conn(mos_type=tx.row.channel_type.value,
                                       row_idx=rows.index_of_same_channel_type(tx.row),
                                       col_idx=tx.col,
                                       fg=tx.fg,
                                       sdir=tx.s_dir.value,
                                       ddir=tx.d_dir.value,
                                       s_net=tx.s_net,
                                       d_net=tx.d_net,
                                       gate_ext_mode=1,
                                       g_via_row=2,
                                       )
            tx.set_ports(g=ports['g'],
                         d=ports[tx.deff.value],
                         s=ports[tx.seff.value])


        # Define transistor properties for schematic
        tx_info = {}
        for tx in transistors:
            tx_info[tx.name] = {
                'w': tx.row.width,
                'th': tx.row.threshold,
                'fg': tx.fg
            }

        self._sch_params = dict(
            lch=params.lch,
            dum_info=self.get_sch_dummy_info(),
            tx_info=tx_info,
        )


class bad_diffpair(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        AnalogBase.__init__(self, temp_db, lib_name, params, used_names, **kwargs)
