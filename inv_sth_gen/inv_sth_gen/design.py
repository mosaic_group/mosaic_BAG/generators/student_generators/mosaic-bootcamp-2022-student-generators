"""
inv_sth
========

"""

from sal.design_base import DesignBase
from .params import Inv_sthParams
from .layout import layout as inv_sth_layout


class design(DesignBase):
    def __init__(self):
        DesignBase.__init__(self)
        self.params = self.parameter_class().defaults(min_lch=self.min_lch)

    @property
    def package(self):
        return "inv_sth_gen"

    @classmethod
    def layout_generator_class(cls):
        """Return the layout generator class"""
        return inv_sth_layout

    @classmethod
    def parameter_class(cls):
        """Return the parameter class"""
        return Inv_sthParams

    # Define template draw and schematic parameters below using property decorators:
    @property
    def params(self) -> Inv_sthParams:
        return self._params

    @params.setter
    def params(self, val: Inv_sthParams):
        self._params = val
