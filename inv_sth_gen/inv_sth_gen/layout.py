from abs_templates_ec.analog_core import AnalogBase
from bag.layout.routing import TrackManager, TrackID
from .params import Inv_sthParams

from sal.transistor import *


class layout(AnalogBase):
    """
    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        AnalogBase.__init__(self, temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None
    
    @classmethod
    def get_params_info(cls):
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='Inv_sthParams parameter object',
        )

    @classmethod
    def get_default_param_values(cls):
        """
        Returns a dictionary containing default parameter values.

        Override this method to define default parameter values.  As good practice,
        you should avoid defining default values for technology-dependent parameters
        (such as channel length, transistor width, etc.), but only define default
        values for technology-independent parameters (such as number of tracks).

        Returns
        -------
        default_params : dict[str, any]
           dictionary of default parameter values.
        """
        return dict(
        )

    @property
    def sch_params(self):
        # type: () -> Dict[str, Any]
        return self._sch_params

    def draw_layout(self):
        params: Inv_sthParams = self.params['params']

        # horiz_conn_layer = self.mos_conn_layer + 1
        vert_conn_layer = self.mos_conn_layer + 2

        tr_manager = TrackManager(self.grid, params.tr_widths, params.tr_spaces)

        # Set up the row information
        row_nmos = Row(name='n',
                       orientation=RowOrientation.MX,
                       channel_type=ChannelType.N,
                       width=params.w_dict['n'],
                       threshold=params.th_dict['n'],
                       wire_names_g=['IN'],
                       wire_names_ds=['OUT', 'OUT2']
                       )

        row_pmos = Row(name='p',
                       orientation=RowOrientation.R0,
                       channel_type=ChannelType.P,
                       width=params.w_dict['p'],
                       threshold=params.th_dict['p'],
                       wire_names_g=['IN', 'IN1'],
                       wire_names_ds=['OUT', 'OUT2']
                       )

        # Define the order of the rows (bottom to top) for this analogBase cell
        rows = RowList([row_nmos, row_pmos])

        # Initialize the transistors in the design
        divide = 1
        fg_n = params.seg_dict['n']
        fg_p = params.seg_dict['p']

        nmos = Transistor(name='n', row=row_nmos, fg=fg_n, seff_net='VSS', deff_net='O')
        pmos = Transistor(name='p', row=row_pmos, fg=fg_p, seff_net='VDD', deff_net='O')

        # Compose a list of all the transistors so it can be iterated over later
        transistors = [nmos, pmos]

        # 3:   Calculate transistor locations
        fg_single = max(nmos.fg, pmos.fg)
        fg_total = fg_single + 2 * params.ndum
        fg_dum = params.ndum

        # Calculate positions of transistors
        nmos.assign_column(offset=fg_dum, fg_col=fg_single, align=TransistorAlignment.CENTER)
        pmos.assign_column(offset=fg_dum, fg_col=fg_single, align=TransistorAlignment.CENTER)

        # 4:  Assign the transistor directions (s/d up vs down)
        nmos.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.DOWN)
        pmos.set_directions(seff=EffectiveSource.S, seff_dir=TransistorDirection.UP)

        n_rows = rows.n_rows
        p_rows = rows.p_rows

        # 5:  Draw the transistor rows, and the transistors
        # Draw the transistor row bases
        self.draw_base(params.lch, fg_total, params.ptap_w, params.ntap_w,
                       n_rows.attribute_values('width'), n_rows.attribute_values('threshold'),
                       p_rows.attribute_values('width'), p_rows.attribute_values('threshold'),
                       tr_manager=tr_manager, wire_names=rows.wire_names_dict(),
                       n_orientations=n_rows.attribute_values('orientation'),
                       p_orientations=p_rows.attribute_values('orientation'),
                       top_layer=params.top_layer,
                       half_blk_x=True, half_blk_y=True,
                       guard_ring_nf=params.guard_ring_nf,
                       )

        # Draw the transistors
        for tx in transistors:
            ports = self.draw_mos_conn(mos_type=tx.row.channel_type.value,
                                       row_idx=rows.index_of_same_channel_type(tx.row),
                                       col_idx=tx.col,
                                       fg=tx.fg,
                                       sdir=tx.s_dir.value,
                                       ddir=tx.d_dir.value,
                                       s_net=tx.s_net,
                                       d_net=tx.d_net,
                                       gate_ext_mode=1,
                                       g_via_row=2,
                                       )
            tx.set_ports(g=ports['g'],
                         d=ports[tx.deff.value],
                         s=ports[tx.seff.value])

        # 6:  Routings
        row_nmos_idx = rows.index_of_same_channel_type(row_nmos)
        row_pmos_idx = rows.index_of_same_channel_type(row_pmos)

        # INPUT
        tid = self.get_wire_id('nch', row_nmos_idx, 'g', wire_name='IN')
        rgate = []
        rgate.append(self.connect_to_tracks([nmos.g], tid))
        tid = self.get_wire_id('pch', row_pmos_idx, 'g', wire_name='IN')
        rgate.append(self.connect_to_tracks([pmos.g], tid))

        tid = TrackID(
            layer_id=vert_conn_layer,
            track_idx=self.grid.coord_to_nearest_track(
                layer_id=vert_conn_layer,
                coord=rgate[0].lower_unit-1,
                half_track=False,
                mode=1,
                unit_mode=True
            ),
            width=tr_manager.get_width(vert_conn_layer, 'sig1')
        )
        rgate.append(self.connect_to_tracks([rgate[0], rgate[1]], tid))
        self.add_pin('I', rgate[2], show=params.show_pins)

        # OUTPUT
        tid = self.get_wire_id('nch', row_nmos_idx, 'ds', wire_name='OUT')
        rout = []
        rout.append(self.connect_to_tracks([nmos.d], tid))
        tid = self.get_wire_id('pch', row_pmos_idx, 'ds', wire_name='OUT')
        rout.append(self.connect_to_tracks([pmos.d], tid))

        tid = TrackID(
            layer_id=vert_conn_layer,
            track_idx=self.grid.coord_to_nearest_track(
                layer_id=vert_conn_layer,
                coord=rout[0].upper_unit,
                half_track=False,
                mode=1,
                unit_mode=True
            ),
            width=tr_manager.get_width(vert_conn_layer, 'sig1')
        )
        rout.append(self.connect_to_tracks([rout[0], rout[1]], tid))
        self.add_pin('O', rout[2], show=params.show_pins)

        # draw dummies
        ptap_wire_arrs, ntap_wire_arrs = self.fill_dummy()
        self.connect_to_substrate('ptap', nmos.s)
        self.connect_to_substrate('ntap', pmos.s)
        # export supplies
        self.add_pin('VSS', ptap_wire_arrs, show=params.show_pins)
        self.add_pin('VDD', ntap_wire_arrs, show=params.show_pins)

        # Define transistor properties for schematic
        tx_info = {}
        for tx in transistors:
            tx_info[tx.name] = {
                'w': tx.row.width,
                'th': tx.row.threshold,
                'fg': tx.fg
            }

        self._sch_params = dict(
            lch=params.lch,
            dum_info=self.get_sch_dummy_info(),
            tx_info=tx_info,
        )


class inv_sth(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        AnalogBase.__init__(self, temp_db, lib_name, params, used_names, **kwargs)
