# MOSAIC-bootcamp-2022-student-generators 

To use with the virtuoso_template of SAL. Not sure which ones are LVS-clean at the moment, but these should work fine if you treat them like the available generators in the parent folder.

Credits:

**AnalogBase+cds_ff_mpt (finfet tech)**

1. (Nifty 1) Inverter, NAND Gate (Taeho)
2. NOR Gate (Seonghyun)
3. Tri-state Inverter, NAND Gate (Youngmin)
4. (Nifty 24, 25) PN, Stable Voltage References (Lawrence)
5. NAND Gate, (Nifty 7, 1) Diff Pair, Inverters (Ryan)
6. (Nifty 7) Sophisticated Current Mirrors (Kevin)


